# Generated by Django 4.0.3 on 2023-01-23 23:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceTechnicianVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tech_name', models.CharField(max_length=200)),
                ('tech_number', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ServiceAppointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.PositiveBigIntegerField()),
                ('customer_name', models.CharField(max_length=200)),
                ('appointment_time', models.DateTimeField()),
                ('service_tech', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='service_tech', to='service_rest.servicetechnicianvo')),
            ],
        ),
    ]
