import React, { useEffect, useState } from 'react';
import './index.css';

function ApptSearch2(props) {
	const [vin, setVin] = useState('');
	// const [customer_name, setCustomer_name] = useState('');
	// const [appointment_time, setappointment_time] = useState('');
	// const [reason, setReason] = useState('');
	// const [service_tech, setService_tech] = useState('');
	// const [techs, setTechs] = useState([]);

	const handleVin = (event) => {
		const value = event.target.value;
		setVin(value);
	};
	// const handlecustomer_name = (event) => {
	// 	const value = event.target.value;
	// 	setCustomer_name(value);
	// };
	// const handleappointment_time = (event) => {
	// 	const value = event.target.value;
	// 	setappointment_time(value);
	// };
	// const handleReason = (event) => {
	// 	const value = event.target.value;
	// 	setReason(value);
	// };
	// const handleService_tech = (event) => {
	// 	const value = event.target.value;
	// 	setService_tech(value);
	// };

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.vin = vin;
		// data.customer_name = customer_name;
		// data.appointment_time = appointment_time;
		// data.reason = reason;
		// data.service_tech = service_tech;

		const locationUrl = 'http://localhost:8080/api/services/';
		const fetchConfig = {
			method: 'get',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		try {
			const response = await fetch(locationUrl, fetchConfig);
			setVin('');
			// setCustomer_name('');
			// setappointment_time('');
			// setReason('');
			// setService_tech('');
		} catch (error) {
			console.log(error);
		}
	};

	// const fetchData = async () => {
	// 	const url = 'http://localhost:8080/api/services/';

	// 	const response = await fetch(url);

	// 	if (response.ok) {
	// 		const data = await response.json();
	// 		setTechs(data.services);
	// 	}
	// };

	// useEffect(() => {
	// 	fetchData();
	// }, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Look Up Appointment By Vin</h1>
					<form onSubmit={handleSubmit} id="create-appointment-form">
						<div className="mb-3">
							<select
								value={vin}
								onChange={handleVin}
								required
								name="vin"
								id="vin"
								className="form-select"
							>
								<option value="">Choose a Vin</option>
								{props.services.map((service) => {
									return (
										<option
											key={service.id}
											value={service.id}
										>
											{service.vin}
										</option>
									);
								})}
							</select>
						</div>

						<button className="btn btn-primary">Look Up</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ApptSearch2;
