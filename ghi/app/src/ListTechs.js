import React from 'react';
import './index.css';
import DeleteTech from './DeleteTech';

function TechList(props) {
	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Technichian Name</th>
					<th>Technichian Number</th>
				</tr>
			</thead>
			<tbody>
				{props.techs.map((tech) => {
					return (
						<tr key={tech.id}>
							<td>{tech.tech_name}</td>
							<td>{tech.tech_number}</td>
							<td>
								<DeleteTech id={tech.id} />{' '}
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default TechList;
