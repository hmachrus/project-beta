import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom";

function SaleList({sales, getSales}) {
    const deleteSale = async (sale) => {
      const saleUrl = `http://localhost:8090/api/sale/${sale.id}/`
      const fetchConfig = {
        method: 'delete'
      };
    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {
        getSales();

    }
    }


    return (
        <>
            <table className="table table-striped table-hover align-middle mt-5">
                <thead>
                    <tr>
                        <th>Sold by:</th>
                        <th>Employee Number:</th>
                        <th>Purchased by:</th>
                        <th>VIN:</th>
                        <th>Sale Price:</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.salesperson.employee_number}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>﹩{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SaleList;
